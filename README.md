#LoggeD

##Version: 0.8-SNAPSHOT

##About

###The license

LoggeD is being released under the **MIT** license.

###The aim

LoggeD is being developed with a specific goal in mind: Fast, yet modular and flexible logging for the D programming language.

A log isn't just a log; There is a message. That message is always a string (sadly. Having a template to represent it posed problems that I cannot fix yet)

###Current status

LoggeD is able to be used in it's current state.

This release (0.8) **will** change the API. Be warned.

####Breaking changes since 0.0.3

* MessageHandler was changed from an interface to an abstract class
* MessageFilter return type was changed from a boolean to FilterStatus (Rejected and Accepted)
* MessageType names changed to conform with the Phobos standards
* LoggeD.VERSION changed to LoggeD.version

###Documentation and examples

This is coming soon. Sorry! I haven't had time to do this yet.

I will update this section once it is complete.