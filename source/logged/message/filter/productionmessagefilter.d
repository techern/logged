module logged.message.filter.productionmessagefilter;

import logged.message.filter.messagefilter;
import logged.message;

/**
 * A MessageFilter that filters out debugging messages
 */
public class ProductionMessageFilter : MessageFilter
{

	/**
	 * Filters debugging messages
	 *
	 * Params:
	 *		message = The message being filtered
	 * Returns: FilterStatus.Accepted if the Message can be used, or FilterStatus.Rejected if it has been filtered out)
	 */
	public override FilterStatus filter(Message message)
	{
		return (message.getType() > MessageType.Debug) ? FilterStatus.Accepted : FilterStatus.Rejected;
	}

}