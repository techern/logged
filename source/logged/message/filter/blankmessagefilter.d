module logged.message.filter.blankmessagefilter;

import logged.message.filter.messagefilter;
import logged.message.message;

/**
 * A MessageFilter that filters out blank messages
 */
public class BlankMessageFilter : MessageFilter
{
	/**
	 * Filters blank messages
	 *
	 * Params:
	 *		message = The message being filtered
	 * Returns: FilterStatus.Accepted if the Message can be used, or FilterStatus.Rejected if it has been filtered out)
	 */
	public override FilterStatus filter(Message message)
	{
		return (message.getMessage().length > 0) ? FilterStatus.Accepted : FilterStatus.Rejected;
	}

}