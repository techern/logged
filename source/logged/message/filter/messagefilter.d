module logged.message.filter.messagefilter;

import logged.message.message;

/**
 * An interface that describes a MessageFilter
 */
public interface MessageFilter
{
	
	/**
	 * Filters the specified Message
	 *
	 * Params:
	 *		message = The message being filtered
	 * Returns: FilterStatus.Accepted if the Message can be used, or FilterStatus.Rejected if it has been filtered out)
	 */
	public FilterStatus filter(Message message);
	
}

/**
 * An enumeration that describes the return status of a MessageFilter
 */
public enum FilterStatus
{

	/**
	 * Denotes that a MessageFilter has deemed a message as acceptable
	 */
	Accepted,

	/**
	 * Denotes that a MessageFilter has deemed a message is not acceptable and will not be logged
	 */
	Rejected

}