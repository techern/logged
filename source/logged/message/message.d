module logged.message.message;

import logged.message.messagetype;

/**
 * A utility class that stores a message
 */
public class Message
{
	/**
	 * Creates a new Message
	 *
	 * Params:
	 *		message = The message that this Message contains
	 *		type =	  The type of Message that this is
	 */
	public this(string message, MessageType type)
	{
		this._message = message;
		this._type = type;
	}
	
	/**
	 * Creates a new Message with the Information MessageType
	 *
	 * Params:
	 *		message = The message that this Message contains
	 */
	public this(string message)
	{
		this(message, MessageType.Information);
	}

	/**
	 * The type of this Message
	 */
	private MessageType _type;
	
	/**
	 * Gets this Message's type
	 *
	 * Returns: The type of Message
	 */
	public MessageType getType()
	{
		return this._type;
	}
	
	/**
	 * Sets this Message's type
	 *
	 * Params:
	 *		type = The type of Message being set
	 */
	public void setType(MessageType type)
	{
		this._type = type;
	}

	/**
	 * The actual message that this class contains
	 */
	private string _message;
	
	/**
	 * Gets the message
	 *
	 * Returns: The raw message
	 */
	public string getMessage()
	{
		return this._message;
	}
	
	/**
	 * Sets a new message
	 *
	 * Params:
	 *		message = The new message being set
	 */
	public void setMessage(string message)
	{
		this._message = message;
	}

}