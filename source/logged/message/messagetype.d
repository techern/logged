module logged.message.messagetype;

/**
 * An enumeration that describes a Message's type
 */
public enum MessageType
{
	/**
	 * A trace message that is usually used for deep debugging. Do not enable by default
	 */
	Trace,
	
	/**
	 * A debugging message. Do not enable by default
	 */
	Debug,
	
	/**
	 * Information that is being generated. This is the equivalent of regular console output
	 */
	Information,
	
	/**
	 * A warning. Something happened that should not normally happen.
	 */
	Warning,
	
	/**
	 * An error. Something has gone wrong. Terribly wrong.
	 */
	Error,
	
	/**
	 * A critical error. The application is clutching to life by a thread. IMMEDIATE ACTION MUST BE TAKEN
	 */
	Critical,
	
	/**
	 * A fatal error. You are too late. Something extremely bad is happening - An unrecoverable error, or perhaps a security breach. 
	 * Maybe you wrote a game and EVERY PLAYER DIED AT THE SAME TIME. Whoops. You should roll back and audit your staff, if so.
	 */
	Fatal

}