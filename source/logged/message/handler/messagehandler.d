module logged.message.handler.messagehandler;

import logged.message.message;

/**
 * An abstract class for handling Messages
 */
public abstract class MessageHandler
{
	/**
	 * Handles a Message
	 *
	 * Params:
	 *		message = The Message being handled
	 *		loggerName = The name of the Logger that requested this handle
	 */
	public void handle(Message message, string loggerName);

}