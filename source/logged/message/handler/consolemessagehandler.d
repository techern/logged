module logged.message.handler.consolemessagehandler;

import std.datetime;
import std.stdio;

import logged.message.message;
import logged.message.handler.messagehandler;

/**
 * A MessageHandler that simply prints to the console
 *
 * TODO: Add colors if possible
 */
public class ConsoleMessageHandler : MessageHandler
{
	/**
	 * Handles a Message
	 *
	 * Params:
	 *		message = The Message being handled
	 *		loggerName = The name of the Logger that requested this handle
	 */
	public override void handle(Message message, string loggerName)
	{
		writefln("%s - [%s] %s: %s", Clock.currTime.toSimpleString(), message.getType(), loggerName, message.getMessage());
	}

}