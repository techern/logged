module logged.logged;

/**
 * A utility class that, at the moment, only contains the version of LoggeD
 */
public static class LoggeD
{
	/**
	 * The current version
	 */
	public shared static string Version = "0.8-SNAPSHOT";
	
}