module logged.logger.logger;

import core.vararg;

import std.array;
import std.datetime;
import std.stdio;
import std.string;

import logged.message;

import logged.message.handler.messagehandler;
import logged.message.handler.consolemessagehandler;

import logged.message.filter.messagefilter;
import logged.message.filter.blankmessagefilter;

/**
 * A class that defines a Logger.
 *
 * Will be expanded upon later. For the initial 0.0.1 release, it writes to the console only
 */
public class Logger
{
	/**
	 * Creates a new anonymous Logger
	 *
	 * Disabled as of 0.8
	 */
	private this()
	{
		this("Anonymous");
	}

	/**
	 * Creates a new Logger
	 *
	 * Params:
	 *		name = The name of this logger
	 */
	public this(string name)
	{
		this(name, [new BlankMessageFilter()]);
	}
	
	/**
	 * Creates a new Logger
	 *
	 * Params:
	 *		name = The name of this logger
	 *		filters = The MessageFilters to use
	 */
	public this(string name, MessageFilter[] filters ...)
	{
		this(name, filters, [new ConsoleMessageHandler()]);
	}
	
	/**
	 * Creates a new Logger
	 *
	 * Params:
	 *		name = The name of this logger
	 *		filters = The MessageFilters to use
	 *		handlers = The MessageHandlers to use
	 */
	public this(string name, MessageFilter[] filters, MessageHandler[] handlers)
	{
		this.name = name;
		this.filters ~= filters;
		this.handlers ~= handlers;
	}

	/**
	 * The name of this Logger
	 */
	private string name;
	
	/**
	 * Gets the name of this Logger
	 *
	 * Returns: The name
	 */
	public string getName()
	{
		return this.name;
	}
	
	/**
	 * The array of MessageHandlers being used by this Logger
	 */
	private MessageHandler[] handlers;
	
	/**
	 * Gets the MessageHandlers being used by this Logger
	 *
	 * Returns: The array of MessageHandlers
	 */
	public MessageHandler[] getHandlers()
	{
		return this.handlers;
	}
	
	/**
	 * Adds a MessageHandler to this Logger
	 *
	 * Params:
	 *		handler = The MessageHandler being added
	 */
	public void addHandler(MessageHandler handler)
	{
		this.handlers ~= handler;
	}
	
	/**
	 * The array of MessageFilters being used by this Logger
	 */
	private MessageFilter[] filters;
	
	/**
	 * Gets the list of MessageFilters used by this Logger
	 *
	 * Returns: The list of MessageFilters
	 */
	public MessageFilter[] getFilters()
	{
		return this.filters;
	}
	
	/**
	 * Adds a filter to the MessageFilters
	 *
	 * Params:
	 * 		filter = The new MessageFilter being added
	 */
	public void addFilter(MessageFilter filter)
	{
		this.filters ~= filter;
	}

	/**
	 * Logs a Message
	 *
	 * Params:
	 *		message = The message being logged
	 */
	public void log(Message message)
	{
		//TODO Implement message handlers
		//TODO Implement formatting
		
		//First, we filter the message
		foreach (MessageFilter filter; getFilters()) {
			if (filter.filter(message) == FilterStatus.Accepted) {
				continue; //It passed. Pass on to the next filter
			} else return; //It was rejected. Don't log this!
		}
		
		foreach (MessageHandler handler; getHandlers()) {
			handler.handle(message, this.getName());
		}
	}
	
	/**
	 * Logs a Message with an attached Exception
	 *
	 * Params:
	 *		message = The message being logged
	 *		attachment = The attached Exception
	 */
	public void log(Message message, Exception attachment)
	{
		log(message);
		log(new Message(attachment.toString(), message.getType()));
	}
	
	/**
	 * Logs a trace message
	 *
	 * Params:
	 *		data = The data being logged
	 */
	public void logTrace(string data)
	{
		log(new Message(data, MessageType.Trace));
	}
	
	/**
	 * Logs a trace message
	 *
	 * Params:
	 *		data = The data being logged
	 *		attachment = The attached Exception
	 */
	public void logTrace(string data, Exception attachment)
	{
		log(new Message(data, MessageType.Trace), attachment);
	}
	
	/**
	 * Logs a debug message
	 *
	 * Params:
	 *		data = The data being logged
	 */
	public void logDebug(string data)
	{
		log(new Message(data, MessageType.Debug));
	}
	
	/**
	 * Logs a debug message
	 *
	 * Params:
	 *		data = The data being logged
	 *		attachment = The attached Exception
	 */
	public void logDebug(string data, Exception attachment)
	{
		log(new Message(data, MessageType.Debug), attachment);
	}
	
	/**
	 * Logs an informational message
	 *
	 * Params:
	 *		data = The data being logged
	 */
	public void logInfo(string data)
	{
		log(new Message(data, MessageType.Information));
	}
	
	/**
	 * Logs an informational message
	 *
	 * Params:
	 *		data = The data being logged
	 *		attachment = The attached Exception
	 */
	public void logInfo(string data, Exception attachment)
	{
		log(new Message(data, MessageType.Information), attachment);
	}
	
	/**
	 * Logs a warning
	 *
	 * Params:
	 *		data = The data being logged
	 */
	public void logWarning(string data)
	{
		log(new Message(data, MessageType.Warning));
	}
	
	/**
	 * Logs a warning
	 *
	 * Params:
	 *		data = The data being logged
	 *		attachment = The attached Exception
	 */
	public void logWarning(string data, Exception attachment)
	{
		log(new Message(data, MessageType.Warning), attachment);
	}
	
	/**
	 * Logs an error
	 *
	 * Params:
	 *		data = The data being logged
	 */
	public void logError(string data)
	{
		log(new Message(data, MessageType.Error));
	}
	
	/**
	 * Logs an error
	 *
	 * Params:
	 *		data = The data being logged
	 *		attachment = The attached Exception
	 */
	public void logError(string data, Exception attachment)
	{
		log(new Message(data, MessageType.Error), attachment);
	}
	
	/**
	 * Logs a critical error
	 *
	 * Params:
	 *		data = The data being logged
	 */
	public void logCritical(string data)
	{
		log(new Message(data, MessageType.Critical));
	}
	
	/**
	 * Logs a critical error
	 *
	 * Params:
	 *		data = The data being logged
	 *		attachment = The attached Exception
	 */
	public void logCritical(string data, Exception attachment)
	{
		log(new Message(data, MessageType.Critical), attachment);
	}
	
	/**
	 * Logs a fatal error
	 *
	 * Params:
	 *		data = The data being logged
	 */
	public void logFatal(string data)
	{
		log(new Message(data, MessageType.Fatal));
	}
	
	/**
	 * Logs a fatal error
	 *
	 * Params:
	 *		data = The data being logged
	 *		attachment = The attached Exception
	 */
	public void logFatal(string data, Exception attachment)
	{
		log(new Message(data, MessageType.Fatal), attachment);
	}
	
	/**
	 * Logs a formatted message
	 *
	 * Params:
	 *		type = The MessageType being logged
	 *		data = The data being logged
	 *		replacements = The Objects being formatted into the data
	 */
	public void logf(Args...)(MessageType type, string data, lazy Args replacements)
	{
		log(new Message(format(data, replacements), type));
	}
	
	/**
	 * Logs a formatted informational message
	 *
	 * Params:
	 *		data = The data being logged
	 *		replacements = The Objects being formatted into the data
	 */
	public void logf(Args...)(string data, lazy Args replacements)
	{
		logf(MessageType.Information, data, replacements);
	}
	

}